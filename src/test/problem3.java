package test;

import java.util.*;

public class problem3 {

    public static void main(String[] args){
        problem3 prob3 = new problem3();
        int[] answer = prob3.prob3(new int[] {1,2,3,4,5,6}, 5);
        System.out.println(Arrays.toString(answer));
    }

    public int[] prob3(int[] numbers, int target){

        Map<Integer, Integer> answer= new HashMap<>();

        for (int i = 0; i < numbers.length; i++) {
            answer.put(numbers[i], i);

            int numbertofind = target - numbers[i];
            if (answer.containsKey(numbertofind) && answer.get(numbertofind) != i){
                return new int[] {i, answer.get(numbertofind)};
            }
        }

        return null;
    }
}
