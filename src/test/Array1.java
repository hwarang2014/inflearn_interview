package test;


import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class Array1 {

    public static void main(String[] args) {
        Array1 array1 = new Array1();
        boolean x = array1.solution1(new int[]{3, 2, 3, 1});
        System.out.println(x);
    }

    private boolean solution1(int[] numbers) {
        Arrays.sort(numbers);
        for (int i = 1; i < numbers.length; i++) {
            if (numbers[i] == numbers[i-1]) {
                    return true;
                }
            }

        return false;
    }

    private boolean solution2(int[] numbers){
        Set<Integer> numberSet = new HashSet<>();
        for (int num : numbers){
            if(numberSet.contains(num)){
                return true;
            } else {
                numberSet.add(num);
            }

        }
        return false;

    }
}
